"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Class AdmEventBus - Handling app events with bus system
 * 	----------------------------------------------------------------------------------------------
 */
class AdmEventBus {

    /**
     * Create a Event-Bus-System.
     */
	constructor(){
		this._bus = new Comment();
	}

    /**
     * Add an event listener.
     * @param {string} event - Name of event
     * @param {function} listener - Function to execute
     */
    addEventListener(event, listener){
        this._bus.addEventListener(event, listener);
    }

    /**
     * Remove an event listener.
     * @param {string} event - Name of event
     * @param {function} listener - Function to execute
     */
    removeEventListener(event, listener){
        this._bus.removeEventListener(event, listener);
    }

    /**
     * Dispatch an event.
     * @param {string} event - Name of event
     * @param {object} data - Data to bind to an event
     */
	dispatchEvent(event, data = {}){
        this._bus.dispatchEvent(new CustomEvent(event, { detail: data }));
    }
}

const eventBus = new AdmEventBus();

export { eventBus, AdmEventBus };
export default AdmEventBus;