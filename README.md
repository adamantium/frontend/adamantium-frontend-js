# Adamantium JS


<a href="https://www.npmjs.com/package/@minddraft/adamantium-frontend-js">
    <img src="https://img.shields.io/npm/v/@minddraft/adamantium-frontend-js.svg" alt="Latest version on npm" />
</a>

**See the official documentation for more infos and examples.**

**[Official Documentation](https://adamantium-js-docs.minddraft.com).**

## Installation

Just install it via npm.

    npm install @minddraft/adamantium-frontend-js