"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Imports
 * 	----------------------------------------------------------------------------------------------
 */
import 'promise-polyfill/src/polyfill';

import {eventBus} from '../event/AdmEventBus';
import {joinPath} from '../utils/AdmUtils';

/**
 *	----------------------------------------------------------------------------------------------
 *	Class Main-Application
 * 	----------------------------------------------------------------------------------------------
 */
class AdmApp{
	
    /**
     * Create a basic adamantium app.
     * @param {Object} config - Configuration object of adamantium app
     */
	constructor(config){
		window.app = this;
		//	Public Properties
		this.theme = localStorage.getItem('adm-theme') || config?.theme || '';
		this.page404 = config?.page404 || '';
		this.root = config?.root || '';
		
		//	Private Properties		
		this._modules = {};
		this._components = {};
		this._processList = [];

		this.setTheme(this.theme); 

		eventBus.addEventListener("error", (event) => {
			console.log(event.detail);
		})		
	}

	/**
     * Initialize app
     */
	 async init(){	
		if( this.pager ){
			this.pager.addEventListener("pageReady", () => {
				if(this.prerender){
					setTimeout(() => {
						this.prerender.setPrerenderReady(true);
					}, 2000)
				}
			});

			if(this.pager.defaultLayout){
				await this.pager.setLayout(this.pager.defaultLayout);
			}
		}
		if( this.auth ) { 
			await this.auth.getAccess();
		}
		if( this.router ) { 
			this.router.addEventListener("routeChanged", (evt) => {
				if(this.prerender?.checkPrerender()){
					this.prerender.setPrerenderStatusCode(200);
				}
				if(this.pager){
					this.pager.loadPage(evt.detail);
				}
			})
			this.router.addEventListener("routeFailed", (evt) => {
				if(this.prerender?.checkPrerender()){
					this.prerender.setPrerenderStatusCode(404);
				}
				if(this.pager && this.page404){
					evt.detail.config = Object.assign({page: this.page404}, evt.detail.config);
					this.pager.loadPage(evt.detail);
				}
			})
			
			if( this.i18n ){	
				/*
				const routes = [...this.router.routes];
				const languages = Array.from(this.i18n.languages);
				languages.shift();
				languages.forEach((lang) => {
					routes.forEach((item) => {
						let config = Object.assign({lang}, item.config);
						let path = joinPath(`/${lang}`, item.path);
						this.router.add(path, config, item.handler);
					});
				});*/

				if(this.i18n.storageLanguage && this.i18n.storageLanguage !== this.i18n.urlLanguage && !this.i18n.disableURLLanguage){
					// if storageLanguage avalibale and different from url language than change language  
					const matchData = this.router.match(location.pathname + location.search + location.hash);
					if(matchData?.result?.config?.langPaths){
						const path = matchData?.result?.config?.langPaths[this.i18n.storageLanguage];
						this.i18n.changeLanguage(this.i18n.storageLanguage, path);
					}else{
						this.i18n.changeLanguage(this.i18n.storageLanguage);
					}
				}
			}
			this.router.navigate(location.pathname + location.search + location.hash);
		 }
	}
	
	/**
     * Reload app
	 * @param {String} newUrl - reload new url  - default current url (optional)  
     */
	reload(newUrl = ""){
		const url = (newUrl) ? newUrl : location.href;
		window.open(url, '_self');
	}

	/**
     * Set app theme.
	 * @param {String} value
     */
	setTheme (value){
		this.theme = value;
		document.documentElement.setAttribute("data-theme", value);
		localStorage.setItem('adm-theme', value);
	}

	/**
     * Add a adamantium module.
	 * @param {string} name - Module key name
     * @param {Class} Module - Class of an adamantium module
	 * @param {Object} config - Configuration object of a module 
	 * @return {Promise} A resolved promise
     */
	async module(name, Module, config){
		const instance = new Module(config);
		Object.defineProperty(this, name, {
			get: () => { return this._modules[name]; },
			set: (module) => { return this._modules[name] = module; }
		}); 
		this[name] = instance;
		if(instance.initComplete){
			await instance.initComplete();
		}
		return Promise.resolve(this[name]);
	}

	/**
     * Add a adamantium component.
	 * @param {string} name - Componentt key name
     * @param {Class} Component - Class of an adamantium component
	 * @param {Object} config - Configuration object of a component 
	 * @return {Promise} A resolved promise
     */
	component(name, Component, config){
		const instance = new Component(config);
		Object.defineProperty(this, name, {
			get: () => { return this._components[name]; },
			set: (component) => { return this._components[name] = component; }
		}); 
		this[name] = instance;
		return Promise.resolve(this[name]);
	}
}

export { AdmApp }