"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Class AdmUtils - Handling app events with bus system
 * 	----------------------------------------------------------------------------------------------
 */
class AdmUtils {

	static documentReady() {
		if (document.readyState === "loading") {
			return new Promise((resolve) => {
				document.addEventListener('DOMContentLoaded', resolve)
			})
		} else {
			return Promise.resolve()
		}
	}

	static joinPath(...parts) {
		let joined = parts.map((part, index) => {
			if (index === 0) {
				return part.replace(/\/+$/, '');	// If it's the first part, remove trailing slashes.
			} else {
				return part.replace(/^\/+/, '');	// For subsequent parts, remove leading slashes.
			}
		}).join('/').replace(/\/+$/, '');
		
		if(!joined){
			joined = '/';
		}
		return joined;
	}
}

const documentReady = AdmUtils.documentReady
const joinPath = AdmUtils.joinPath

export { documentReady, joinPath, AdmUtils }