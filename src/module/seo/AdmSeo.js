"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Imports
 * 	----------------------------------------------------------------------------------------------
 */


/**
 *	----------------------------------------------------------------------------------------------
 *	Class AdmSeo - Handling app seo 
 * 	----------------------------------------------------------------------------------------------
 */
class AdmSeo{

    /**
     * Create a Router.
     * @param {object} config - Configuration object of seo
	 * @example 
	 * //Example 1:
	 * config = {
	 *	locales: ['de', 'en'],
	 * 	initComplete =  () => {} || null;
	 * };
     */
	constructor(config = {}){
		this.locales = new Set(config.locales || []); 
		this.initComplete = config.initComplete || null;
	}

    /**
     * Set HTML meta data.
	 * @param {object} metadata - Object with HTML metadatas	 
	 * //Example 1:
	 * metadata = {
	 *		META_LANG: "",
	 *		META_URL_DEFAULT: "",
	 *		META_URL_CANONICLE: "",
	 * 		META_URL_LANG_LIST: [],
	 *		META_TITLE: "",
	 *		META_DESCRIPTION: "",
	 *		META_OG_TITLE: "",
	 *		META_OG_DESCRIPTION: "",
	 *		META_OG_IMAGE: "",
	 *		META_OG_URL: "",
	 *		META_TWITTER_CARD: "",
	 *		META_TWITTER_SITE: "",
	 *		META_TWITTER_CREATOR: ""
	 * };
     */
	setMetaData(metadata){
		var d = new Date();

		// SEO tags
		if (document.querySelector('html[lang]')) { 
			document.querySelector('html').setAttribute("lang", metadata?.META_LANG || ""); 
		};
		if (document.querySelector('link[hreflang="x-default"]')) { 
			document.querySelector('link[hreflang="x-default"]').setAttribute("href", metadata?.META_URL_DEFAULT || ""); 
		};
		if (document.querySelector(`link[rel="canonical"]`)) { 
			document.querySelector(`link[rel="canonical"]`).setAttribute("href", metadata?.META_URL_CANONICLE || ""); 
		};
		if(metadata?.META_URL_LANG_LIST?.length){
			metadata?.META_URL_LANG_LIST.forEach((item) => {
				document.querySelector(`link[hreflang="${item.lang}"]`).setAttribute("href", item.url);
			});
		}else{
			this.locales.forEach((locale) => {
				if (document.querySelector(`link[hreflang="${locale}"]`)) { 
					const routeParts = location.pathname.split("/");
					if(this.locales.has(routeParts[1])){
						routeParts.splice(1, 1);
					}
					if(locale !== this.locales.values().next().value){
						routeParts.splice(1, 0, locale);
						if(!routeParts[routeParts.length - 1]){
							routeParts.pop();
						}
					}
					document.querySelector(`link[hreflang="${locale}"]`).setAttribute("href", `${location.origin}${routeParts.join('/')}`);
				};
			})
		}

		if (document.title) {
			document.title = metadata?.META_TITLE || "";
		};
		if (document.querySelector('meta[name="title"]')) {
			document.querySelector('meta[name="title"]').setAttribute("content", metadata?.META_TITLE || "");
		};
		if (document.querySelector('meta[name="description"]')) {
			document.querySelector('meta[name="description"]').setAttribute("content", metadata?.META_DESCRIPTION || "");
		};

		// og tags
		if (document.querySelector('meta[property="og:title"]')) {
			document.querySelector('meta[property="og:title"]').setAttribute("content", metadata?.META_OG_TITLE || "");
		};
		if (document.querySelector('meta[property="og:description"]')) {
			document.querySelector('meta[property="og:description"]').setAttribute("content", metadata?.META_OG_DESCRIPTION || "");
		};
		if (document.querySelector('meta[property="og:image"]')) {
			document.querySelector('meta[property="og:image"]').setAttribute("content", metadata?.META_OG_IMAGE + '?v=' + d.getTime() || "");
		};
		if (document.querySelector('meta[property="og:image:secure_url"]')) {
			document.querySelector('meta[property="og:image:secure_url"]').setAttribute("content", metadata?.META_OG_IMAGE + '?v=' + d.getTime() || "");
		};
		if (document.querySelector('meta[property="og:url"]')) {
			document.querySelector('meta[property="og:url"]').setAttribute("content", metadata?.META_OG_URL || "");
		};

		// twitter cards
		if (document.querySelector('meta[name="twitter:card"]')) {
			document.querySelector('meta[name="twitter:card"]').setAttribute("content", metadata?.META_TWITTER_CARD || "");
		};
		if (document.querySelector('meta[name="twitter:site"]')) {
			document.querySelector('meta[name="twitter:site"]').setAttribute("content", metadata?.META_TWITTER_SITE || "");
		};
		if (document.querySelector('meta[name="twitter:creator"]')) {
			document.querySelector('meta[name="twitter:creator"]').setAttribute("content", metadata?.META_TWITTER_CREATOR || "");
		};

		if(metadata?.STRUCTURED_DATA){{
			const script = document.createElement('script');
			script.setAttribute('type', 'application/ld+json');
			script.textContent = metadata.STRUCTURED_DATA;
			document.head.appendChild(script);
		}}
	}
}

export { AdmSeo }