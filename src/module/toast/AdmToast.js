"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Imports
 * 	----------------------------------------------------------------------------------------------
 */
import iziToast from "izitoast";
import "izitoast/dist/css/iziToast.css";

/**
 *	----------------------------------------------------------------------------------------------
 *	Class AdmToast 
 * 	----------------------------------------------------------------------------------------------
 */
class AdmToast{

    static info(msg){
        iziToast.info({"class": 'gmsg info', "title": msg, "theme": 'light', "animateInside": false, "position": "topRight"});
    }

    static warning(msg){
        iziToast.warning({"class": 'gmsg warning', "title": msg, "theme": 'light', "animateInside": false, "position": "topRight"});
    }

    static error(msg){
        iziToast.error({"class": 'gmsg error', "title": msg, "theme": 'light', "animateInside": false, "position": "topRight"});
    }

    static success(msg){
        iziToast.success({"class": 'gmsg success', "title": msg, "theme": 'light', "animateInside": false, "position": "topRight"});
    }
}

export {AdmToast}
