"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Imports
 * 	----------------------------------------------------------------------------------------------
 */
import AdmEventBus from '../event/AdmEventBus';
 
/**
 *	----------------------------------------------------------------------------------------------
 *	Class Page Handling
 * 	----------------------------------------------------------------------------------------------
 */
 class AdmPager extends AdmEventBus{ 
	
    /**
     * Create a basic adamantium app.
     * @param {Object} config - Configuration object of adamantium app
     */
	constructor(config = {}){
		super();
		this.appendTo = config.appendTo;
		this.loader = config.loader;
		this.defaultLayout = config.defaultLayout;
		this.initComplete = config.initComplete || null;

		this.currentLayout = null;
		this.currentPage =  null;

		this._currentPageModule =  null;
		this._currentLayoutModule =  null;	
	}

    /**
     * Load page with external loader.
	 * @access private 
	 * @param {Object} data - page data
     */
	async loadPage(data){
		const Module = await this.loader(data);
		if(Module){
			this.setPage(Module.Page, data);
		}
	}

    /**
     * Set loaded page.
	 * @access public 
     * @param {Class} Page - Class of page
	 * @param {Object} pageData - page route data
     */
	async setPage(Page, pageData){ 
		if(this._currentPageModule === Page){
			if(this.currentPage.load){
				await this.currentPage.load(pageData);
			}
			if(this.currentPage.show){
				await this.currentPage.show();
			}
		}
		else{ 	
			if(this.currentPage){
				await this.removePage();
			}
			if( pageData?.config?.layout ){
				if( pageData.config.layout != this._currentLayoutModule ){
					await this.setLayout(pageData.config.layout);
				}		
			}
			else if( this.defaultLayout && this.defaultLayout != this._currentLayoutModule ){
				await this.setLayout(this.defaultLayout);
			}
		
			await this.addPage(Page, pageData);

			if(this.currentPage.load){
				await this.currentPage.load(pageData);
			}
			if(this.currentPage.show){
				await this.currentPage.show();
			}			
		}
		this.dispatchEvent("pageReady");
	}

	async addPage(Page, pageData){
		return new Promise(async (resolve) => {
			this._currentPageModule = Page;
			this.currentPage = new Page(pageData);
			if(this.currentPage.preInit){
				await this.currentPage.preInit();
			}
			document.querySelector(this.appendTo).appendChild(this.currentPage); 

			if(this.currentPage.init){
				this.currentPage.init(resolve);
			}else{
				this.currentPage.init = resolve;
			}
		})
	}

	async removePage(){
		if(this.currentPage.unload){
			await this.currentPage.unload();
		}
		if(this.currentPage.hide){
			await this.currentPage.hide();
		}		
		this.currentPage.remove();
	}

	/**
     * Set a page layout.
	 * @access public 
     * @param {Class} Layout - Class of a layout
	 * @return {Promise} A resolved promise
     */
	setLayout(Layout){
		const root = (window.app.root) ? document.querySelector(window.app.root) : document.body;
		root.innerHTML = "";		
		return new Promise((resolve) => {
			this._currentLayoutModule = Layout;
			this.currentLayout = new Layout();
			root.appendChild(this.currentLayout);

			if(this.currentLayout.init){
				this.currentLayout.init(resolve);
			}else{
				this.currentLayout.init = resolve;
			}
		})	
	}
}

export { AdmPager }