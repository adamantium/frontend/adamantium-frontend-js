"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Imports
 * 	----------------------------------------------------------------------------------------------
 */


/**
 *	----------------------------------------------------------------------------------------------
 *	Class AdmI18n - internationalization Module
 * 	----------------------------------------------------------------------------------------------
 */
 class AdmI18n {

    /**
     * Create an adamantium internationalization instance.
     */
	constructor(options = {}){
        this.languages = new Set(options.languages || ['en']);
        this.currentLanguage = "";
        this.storageLanguage = "";
        this.urlLanguage = "";
        this.initComplete = options.initComplete || null;
        this.disableURLLanguage = false;
        
        if(options.disableURLLanguage){
            this.disableURLLanguage = true;
        }

        this.detectLanguage();
	}

    /**
     * Detect current language.
     * @return {string} language iso code
     */
     detectLanguage(){
        // First: check language in local storage - should only be set from user language switch component
        const storageLanguage = localStorage.getItem('language');
        if(this.languages.has(storageLanguage)){
            this.storageLanguage = storageLanguage;
        }else{
            localStorage.removeItem("language");
        }

        // Second: check language in url - e.g. https://... .com/<code>/example or https://... .com/example = default language
        const urlLanguage = location.pathname.split("/")[1];
        if(this.languages.has(urlLanguage)){
            this.urlLanguage = urlLanguage;
        }else{
            this.urlLanguage = this.getDefaultLanguage();
        }
    
        this.currentLanguage = this.storageLanguage || this.urlLanguage;
        return this.currentLanguage;
    }

    /**
     * Get current language.
     * @return {string} language iso code
     */
    getLanguage(){
        return this.currentLanguage;
    }

    /**
     * Set current language.
     * @param {string} code language iso-code
     * @return {boolean} false -> language not available
     */
    setLanguage(code){
        if(!this.languages.has(code)){ return false; }
        this.currentLanguage = code;
        if(globalThis.localStorage){
            globalThis.localStorage.setItem('language', code);
        }
        return true;
    }

    /**
     * Check if language ids default language.
     * @return {boolean} true || false
     */
     isDefaultLanguage(){
        return (this.getDefaultLanguage() === this.currentLanguage);
    }

    /**
     * Get default language - first language in languages array list
     * @return {string} language iso code
     */
     getDefaultLanguage(){
        return this.languages.values().next().value;
    }

    /**
     * Change language (be set from user language switch component) and reload page.
     * @param {string} code language iso-code
     * @param {string} path translated path (optional)
     */
    changeLanguage(code, path = ''){
        if(this.setLanguage(code)){
            // Prepare reload path
            const routeParts = (path || location.pathname + location.search + location.hash).split("/");
            if(!this.disableURLLanguage){
                // If code is default language than remove language from url if exists 
                if(this.getDefaultLanguage() === code){
                    if(this.languages.has(routeParts[1])){
                        routeParts.splice(1, 1);
                    }
                }
                // code have to be in url --> add or replace language in url 
                else{
                    if(this.languages.has(routeParts[1])){
                        routeParts[1] = code;
                    }else{
                        routeParts.splice(1, 0, code);
                    }
                }
                if(!routeParts[routeParts.length - 1]){
                    routeParts.pop();
                }
            }
            const reloadPath = routeParts.join("/") || "/";
            app.reload(reloadPath);
        }else{
            if(globalThis.console){
                globalThis.console.log("AdmI18n::changeLanguage - Language not available" );
            }
        }
    }
}

export { AdmI18n };