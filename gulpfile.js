'use strict';

/**
 * 	------------------------------------------------------------------
 * 	Imports
 * 	------------------------------------------------------------------
 */
const gulp 				= require('gulp');
const child 			= require('child_process');
const fs 				= require('fs');
const semver 			= require('semver');
const jsonfile  		= require('jsonfile');
const jsdoc 			= require('gulp-jsdoc3');
const jsdoc2md 			= require('jsdoc-to-markdown')
  
 const _publish = function (callback) {
	const data 				= jsonfile.readFileSync('package.json');
	const currentVersion 	= data.version;
	const nextVersion 		= semver.inc(currentVersion, 'patch');
	const currentBranch 	= child.execSync(`git rev-parse --abbrev-ref HEAD`, {encoding: 'utf8'}).trim();

	if(currentBranch !== 'main'){
		console.error(`Current branch is: ${currentBranch} Please change to 'main' branch to build and publish`);
		process.exit(1);
	}

	if(child.execSync(`git status -s`, {encoding: 'utf8'}).trim()){
		child.execSync(`git add --all`);
		child.execSync(`git commit -m 'prepare-release'`);
	}
	child.execSync(`git pull --quiet origin main`);
	child.execSync(`git pull --quiet origin production`);

	const tagName = `v${currentVersion}`;
	child.execSync(`git tag -a ${tagName} -m "New release"`);
	child.execSync(`git push --quiet origin --tags`);

	data.version = nextVersion;
	jsonfile.writeFileSync('package.json', data, { spaces: 4 });	

	child.execSync(`git add --all`);
	child.execSync(`git commit -m "update version"`);
	child.execSync(`git push --quiet origin --all`);
	callback();
}

 const _deploy = function (callback) {
	const latestTag = child.execSync(`git tag | sort -V | tail -1`, {encoding: 'utf8'}).trim();
	console.log(latestTag);
	child.execSync(`git checkout ${latestTag}`);
	child.execSync(`npm publish`);
	child.execSync(`git checkout main`);
	callback();
};

 const _docs = function (callback) {
	const config = jsonfile.readFileSync('jsdoc.json');
    gulp.src(['index.js', './src/**/*.js', ], {read: false})
        .pipe(jsdoc(config, callback));
};

gulp.task('docs', _docs);
gulp.task('publish', _publish);
gulp.task('deploy', _deploy);
gulp.task('default', gulp.series(
	'docs',
	'publish',
	'deploy'
))
