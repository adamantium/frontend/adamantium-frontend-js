"use strict"

/**
 *	----------------------------------------------------------------------------------------------
 *	Imports
 * 	----------------------------------------------------------------------------------------------
 */


/**
 *	----------------------------------------------------------------------------------------------
 *	Class AdmPrerender - Handling prerenderer.io 
 * 	----------------------------------------------------------------------------------------------
 */
class AdmPrerender{

    /**
     * Create a Prerenderer module.
     * @param {object} config - Configuration object of seo
	 * @example 
	 * config = {
	 * 	initComplete =  () => {} || null;
	 * };
     */
	constructor(config = {}){
		this.initComplete = config.initComplete || null;
	}

    /**
     * Check if user agent from prerender
	 * - must include this 'Prerender (+https://github.com/prerender/prerender)'
	 * @param {boolean} value - true|false	
     */
	checkPrerender(){
		return window.navigator.userAgent.includes('Prerender')
	}

    /**
     * Set prerender flag
	 * - helps prerender to detect page finish loading
	 * @param {boolean} value - true|false	
     */
	setPrerenderReady(value){
		window.prerenderReady = value;
	}

    /**
     * Set prerender-status-code meta tag
	 * - helps prerender to detect http status
	 * @param {boolean} value - true|false	 
     */
	setPrerenderStatusCode(statusCode){
		let metaElem = document.querySelector('meta[name=prerender-status-code]');
		if(metaElem){
			document.querySelector('head').removeChild(metaElem);
		}
		metaElem = document.createElement("meta");
		metaElem.setAttribute('name', 'prerender-status-code');
		metaElem.setAttribute('content', statusCode);
		document.querySelector('head').appendChild(metaElem); 
	}
}

export { AdmPrerender }