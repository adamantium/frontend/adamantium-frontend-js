<a href="https://www.npmjs.com/package/@minddraft/adamantium-frontend-js">
    <img src="https://img.shields.io/npm/v/@minddraft/adamantium-frontend-js.svg" alt="Latest version on npm" />
</a>

## Description

AdamantiumJS is a small lightweight JavaScript library for building frontends.

## Installation

Just install it via npm.

    npm install @minddraft/adamantium-frontend-js